package com.rbonolis.paging.service;

import com.rbonolis.paging.models.TelemetryStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PagingAlertServiceImplTest {

    static String testData = """
                20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
                20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
                20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT
                20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
                20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT
                20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT
                20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT
                20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT
                20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
                20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT
                20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT
                20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT
                20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT
                20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT
                """;

    PagingAlertServiceImpl service;

    @BeforeEach
    void setup() {
        service = new PagingAlertServiceImpl();
    }
    @Test
    void testParse() {

        List<TelemetryStatus> statusList = service.parseTelemetryStatus(testData);

        assertThat(statusList.size()).isEqualTo(14);
    }

    @Test
    void testSample() {
        String result = service.generateReportFromData(3, 3, 5, testData);

        String expectation = """
                [ {
                  "satelliteId" : 1000,
                  "severity" : "RED HIGH",
                  "component" : "TSTAT",
                  "timestamp" : "2018-01-01T23:01:38.001Z"
                }, {
                  "satelliteId" : 1000,
                  "severity" : "RED LOW",
                  "component" : "BATT",
                  "timestamp" : "2018-01-01T23:01:09.521Z"
                } ]""";

        assertThat(expectation).isEqualToNormalizingNewlines(result);
    }

    @Test
    void testNoViolations() {
        String testDataNone = """
                20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
                20180101 23:01:09.521|1000|17|15|9|8|9.8|BATT
                20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT
                20180101 23:01:38.001|1000|101|98|25|20|99.9|TSTAT
                20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT
                20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT
                20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT
                20180101 23:02:11.302|1000|17|15|9|8|8.7|BATT
                20180101 23:03:03.008|1000|101|98|25|20|98.7|TSTAT
                20180101 23:03:05.009|1000|101|98|25|20|98.2|TSTAT
                20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT
                20180101 23:04:11.531|1000|17|15|9|8|8.9|BATT
                20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT
                20180101 23:05:07.421|1001|17|15|9|8|8.9|BATT
                """;

        String result = service.generateReportFromData(3, 3, 5, testDataNone);

        String expectation = "[ ]";

        assertThat(expectation).isEqualToNormalizingNewlines(result);
    }

    @Test
    void testScatteredViolations() {
        String testDataScattered = """
                20180101 23:01:05.001|1001|101|98|25|20|101.9|TSTAT
                20180101 23:01:09.521|1000|17|15|9|8|9.8|BATT
                20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT
                20180101 23:01:38.001|1000|101|98|25|20|99.9|TSTAT
                20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT
                20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT
                20180101 23:02:10.021|1001|101|98|25|20|102.4|TSTAT
                20180101 23:02:11.302|1000|17|15|9|8|8.7|BATT
                20180101 23:03:03.008|1000|101|98|25|20|98.7|TSTAT
                20180101 23:03:05.009|1000|101|98|25|20|98.2|TSTAT
                20180101 23:09:06.017|1001|101|98|25|20|103.9|TSTAT
                20180101 23:09:11.531|1000|17|15|9|8|8.9|BATT
                20180101 23:15:05.021|1001|101|98|25|20|104.9|TSTAT
                20180101 23:15:07.421|1001|17|15|9|8|8.9|BATT
                """;

        String result = service.generateReportFromData(3, 3, 5, testDataScattered);

        String expectation = "[ ]";

        assertThat(expectation).isEqualToNormalizingNewlines(result);
    }

    @Test
    void testManyViolations() {
        String testDataMany = """
                20180101 23:01:05.001|1001|101|98|25|20|101.9|TSTAT
                20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
                20180101 23:01:26.011|1001|101|98|25|20|102.8|TSTAT
                20180101 23:01:38.001|1000|101|98|25|20|104.9|TSTAT
                20180101 23:01:49.021|1000|101|98|25|20|105.9|TSTAT
                20180101 23:02:09.014|1001|101|98|25|20|101.3|TSTAT
                20180101 23:02:10.021|1001|101|98|25|20|103.4|TSTAT
                20180101 23:02:11.302|1000|17|15|9|8|7.6|BATT
                20180101 23:03:03.008|1000|101|98|25|20|101.7|TSTAT
                20180101 23:03:05.009|1000|101|98|25|20|102.2|TSTAT
                20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT
                20180101 23:04:11.531|1000|17|15|9|8|7.6|BATT
                20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT
                20180101 23:05:07.421|1001|17|15|9|8|7.3|BATT
                """;

        String result = service.generateReportFromData(3, 3, 5, testDataMany);

        String expectation = """
                [ {
                  "satelliteId" : 1001,
                  "severity" : "RED HIGH",
                  "component" : "TSTAT",
                  "timestamp" : "2018-01-01T23:01:05.001Z"
                }, {
                  "satelliteId" : 1001,
                  "severity" : "RED HIGH",
                  "component" : "TSTAT",
                  "timestamp" : "2018-01-01T23:01:26.011Z"
                }, {
                  "satelliteId" : 1000,
                  "severity" : "RED HIGH",
                  "component" : "TSTAT",
                  "timestamp" : "2018-01-01T23:01:38.001Z"
                }, {
                  "satelliteId" : 1000,
                  "severity" : "RED HIGH",
                  "component" : "TSTAT",
                  "timestamp" : "2018-01-01T23:01:49.021Z"
                }, {
                  "satelliteId" : 1000,
                  "severity" : "RED LOW",
                  "component" : "BATT",
                  "timestamp" : "2018-01-01T23:01:09.521Z"
                } ]""";

        assertThat(expectation).isEqualToNormalizingNewlines(result);
    }
}
