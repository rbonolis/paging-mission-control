package com.rbonolis.paging;

import com.rbonolis.paging.service.PagingAlertService;
import com.rbonolis.paging.service.PagingAlertServiceImpl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

// In a real scenario, this Launcher would instead launch the PagingAlertService as something that API calls
// could be made to. Considering the scope of the programming challenge however, it will just load a file
// to show how the service would work
public class Launcher {

    static int redHighLimit = 3;
    static int redLowLimit = 3;
    static int interval = 5;
    public static void main(String[] args) {

        if (args.length < 1) {
            System.out.println("Please provide an input file");
            System.exit(1);
        }

        Path filePath = Path.of(args[0]);
        String data;
        try {
            data = Files.readString(filePath);
        } catch (IOException e) {
            System.out.println("Error parsing file");
            throw new RuntimeException(e);
        }

        PagingAlertService alertService = new PagingAlertServiceImpl();
        System.out.println(alertService.generateReportFromData(redHighLimit, redLowLimit, interval, data));
    }
}