package com.rbonolis.paging.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.rbonolis.paging.models.AlertMessage;
import com.rbonolis.paging.models.AlertMessageSerializer;
import com.rbonolis.paging.models.Component;
import com.rbonolis.paging.models.TelemetryStatus;
import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class PagingAlertServiceImpl implements PagingAlertService {
    private static final SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
    private final ObjectMapper mapper;
    private final Map<Integer, Queue<TelemetryStatus>> violationQueue;
    public PagingAlertServiceImpl() {
        violationQueue = new HashMap<>();
        SimpleModule module = new SimpleModule();
        module.addSerializer(AlertMessage.class, new AlertMessageSerializer());
        mapper = new ObjectMapper();
        mapper.registerModule(module);
    }

    @Override
    public String generateReportFromData(int redHighLimitAllowance, int redLowLimitAllowance, int interval, String data) {
        List<TelemetryStatus> statuses = parseTelemetryStatus(data);
        List<AlertMessage> alerts = new ArrayList<>();

        // one of the best ways to do this requires an assumption of the data being in-order, so sort by date
        Collections.sort(statuses);

        for(TelemetryStatus telemetryStatus : statuses) {
            boolean violation = false;

            // Set violation if battery is below threshold or thermostat is above threshold
            switch(telemetryStatus.getComponent()) {
                case BATT -> violation = telemetryStatus.getRedLowLimit() > telemetryStatus.getRawValue();
                case TSTAT -> violation = telemetryStatus.getRedHighLimit() < telemetryStatus.getRawValue();
            }

            if (violation) {
                Queue<TelemetryStatus> queue = violationQueue.getOrDefault(telemetryStatus.hashCode(), new LinkedList<>());

                TelemetryStatus first = queue.peek();
                if (first == null) {
                    queue.add(telemetryStatus);
                    violationQueue.put(telemetryStatus.hashCode(), queue);
                } else {
                    // check if the new violation is within interval of the first one
                    if (DateUtils.addMinutes(first.getTimestamp(), interval).getTime() > telemetryStatus.getTimestamp().getTime()) {
                        queue.add(telemetryStatus);

                        // check to see if it has hit allowance. If so, make an alert and pop the first
                        switch(first.getComponent()) {
                            case BATT -> {
                                if (queue.size() == redLowLimitAllowance) {
                                    alerts.add(first.toAlertMessage());
                                    queue.remove();
                                }
                            }
                            case TSTAT -> {
                                if (queue.size() == redHighLimitAllowance) {
                                    alerts.add(first.toAlertMessage());
                                    queue.remove();
                                }
                            }

                        }
                    } else {
                        // if it is not within the interval, remove the first in queue and add the newest one
                        queue.remove();
                        queue.add(telemetryStatus);
                        violationQueue.put(telemetryStatus.hashCode(), queue);
                    }
                }
            }
        }

        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(alerts);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * converts a string of data into TelemetryStatus objects
     * @param data string of data to convert
     * @return list of TelemetryStatus objects
     */
    public List<TelemetryStatus> parseTelemetryStatus(String data) {
        // normally there would be much more error-checking here, but the instructions specifically state
        // to assume no errors within the file and to omit that error handling

        List<TelemetryStatus> statuses = new ArrayList<>();
        Scanner scanner = new Scanner(data);

        while (scanner.hasNextLine()) {
            String[] telemetryLine = scanner.nextLine().split("\\|");
            try {
                statuses.add(new TelemetryStatus(
                        inputDateFormat.parse(telemetryLine[0]),
                        Integer.parseInt(telemetryLine[1]),
                        Double.parseDouble(telemetryLine[2]),
                        Double.parseDouble(telemetryLine[3]),
                        Double.parseDouble(telemetryLine[4]),
                        Double.parseDouble(telemetryLine[5]),
                        Double.parseDouble(telemetryLine[6]),
                        Component.valueOf(telemetryLine[7])));
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }

        return statuses;
    }
}
