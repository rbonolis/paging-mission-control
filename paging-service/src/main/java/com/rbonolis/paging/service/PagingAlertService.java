package com.rbonolis.paging.service;

// This is put into a service to better follow the flow of microservice architecture, without more context on the
// project this service is fairly barebones, but meant to be easily modified and expanded
public interface PagingAlertService {
    /**
     * Accepts limit parameters and intervals for a set of pipe-delimited records with the format of
     * <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
     *  and returns json-formatted alerts
     * @param redHighLimit reading limit where exceeding this value X times within the interval will generate an alert
     * @param redLowLimit reading limit where being under this value X within the interval will generate an alert
     * @param interval interval time in minutes to check for violations
     * @return json-formatted alerts
     */
    String generateReportFromData(int redHighLimit, int redLowLimit, int interval, String data);
}
