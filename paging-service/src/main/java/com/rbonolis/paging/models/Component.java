package com.rbonolis.paging.models;

public enum Component {
    BATT("RED LOW"),
    TSTAT("RED HIGH");

    private final String severity;

    Component(String severity) {
        this.severity = severity;
    }

    public String getSeverity() {
        return severity;
    }
}
