package com.rbonolis.paging.models;

import java.util.Date;

public class AlertMessage {
    private final int satelliteId;
    private final Component component;
    private final Date timestamp;

    public AlertMessage(int satelliteId, Component component, Date timestamp) {
        this.satelliteId = satelliteId;
        this.component = component;
        this.timestamp = timestamp;
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public Component getComponent() {
        return component;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}

