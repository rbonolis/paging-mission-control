package com.rbonolis.paging.models;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.text.SimpleDateFormat;

public class AlertMessageSerializer extends StdSerializer<AlertMessage> {
    private static final SimpleDateFormat outputDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    public AlertMessageSerializer() {
        this(null);
    }

    public AlertMessageSerializer(Class<AlertMessage> t) {
        super(t);
    }

    @Override
    public void serialize(AlertMessage alertMessage, JsonGenerator jsonGenerator, SerializerProvider serializer) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("satelliteId", alertMessage.getSatelliteId());
        jsonGenerator.writeStringField("severity", alertMessage.getComponent().getSeverity());
        jsonGenerator.writeStringField("component", alertMessage.getComponent().name());
        jsonGenerator.writeStringField("timestamp", outputDateFormat.format(alertMessage.getTimestamp()));
        jsonGenerator.writeEndObject();
    }
}
