package com.rbonolis.paging.models;

import java.util.Date;
import java.util.Objects;

public class TelemetryStatus implements Comparable<TelemetryStatus> {
    private final Date timestamp;
    private final Integer satelliteId;
    private final Double redHighLimit;
    private final Double yellowHighLimit;
    private final Double yellowLowLimit;
    private final Double redLowLimit;
    private final Double rawValue;
    private final Component component;

    public TelemetryStatus(Date timestamp, Integer satelliteId, Double redHighLimit, Double yellowHighLimit, Double yellowLowLimit, Double redLowLimit, Double rawValue, Component component) {
        this.timestamp = timestamp;
        this.satelliteId = satelliteId;
        this.redHighLimit = redHighLimit;
        this.yellowHighLimit = yellowHighLimit;
        this.yellowLowLimit = yellowLowLimit;
        this.redLowLimit = redLowLimit;
        this.rawValue = rawValue;
        this.component = component;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public Integer getSatelliteId() {
        return satelliteId;
    }

    public Double getRedHighLimit() {
        return redHighLimit;
    }

    public Double getYellowHighLimit() {
        return yellowHighLimit;
    }

    public Double getYellowLowLimit() {
        return yellowLowLimit;
    }

    public Double getRedLowLimit() {
        return redLowLimit;
    }

    public Double getRawValue() {
        return rawValue;
    }

    public Component getComponent() {
        return component;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TelemetryStatus that = (TelemetryStatus) o;
        return Objects.equals(timestamp, that.timestamp) && Objects.equals(satelliteId, that.satelliteId) && Objects.equals(redHighLimit, that.redHighLimit) && Objects.equals(yellowHighLimit, that.yellowHighLimit) && Objects.equals(yellowLowLimit, that.yellowLowLimit) && Objects.equals(redLowLimit, that.redLowLimit) && Objects.equals(rawValue, that.rawValue) && component == that.component;
    }

    // For use in hashmap as key, since we want to keep track of any that have the same id and component
    @Override
    public int hashCode() {
        return Objects.hash(satelliteId, component);
    }

    @Override
    public int compareTo(TelemetryStatus o) {
        return getTimestamp().compareTo(o.getTimestamp());
    }

    public AlertMessage toAlertMessage() {
        return new AlertMessage(getSatelliteId(), getComponent(), getTimestamp());
    }
}
